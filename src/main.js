import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';

const title = new Component( 'h1','', 'La carte' );
document.querySelector('.pageTitle').innerHTML = title.render();

const img = new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300');
//const img = new Component( 'img', {name:'src', value:'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'} );


document.querySelector( '.pageContent' ).innerHTML = img.render();

console.log(img.render());


