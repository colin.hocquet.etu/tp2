export default class Component{
	tagName;
	attribute;
	children;
	constructor(tagName,attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}	
	render(){
		if(this.children==undefined){
			return `<${this.tagName} ${this.renderAttribute()} />`;
        }
       /* if(Component instanceof children){
            return `<${this.tagName} ${this.renderChildren()} />`;
        }*/
		return '<'+this.tagName+'>'+this.children+'</'+this.tagName+'>';
	}
	renderAttribute(){
		return  ` ${this.attribute.name} = \"${this.attribute.value}\"` ;
    }
    renderChildren(){
        
    }
}
